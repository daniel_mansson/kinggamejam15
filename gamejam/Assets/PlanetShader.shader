﻿Shader "Unlit/PlanetShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
	_DataTex("Texture", 2D) = "red" {}
	}
		SubShader
	{
		Tags{ "RenderType" = "Transparent" }
		LOD 100
		Lighting Off
		ZWrite On
		//ZWrite On  // uncomment if you have problems like the sprite disappear in some rotations.
		Cull back
		Blend SrcAlpha OneMinusSrcAlpha
		//AlphaTest Greater 0.001  // uncomment if you have problems like the sprites or 3d text have white quads instead of alpha pixels.
		Tags{ Queue = Transparent }

		Pass
	{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
		// make fog work

#include "UnityCG.cginc"

	struct appdata
	{
		float4 vertex : POSITION;
		float2 uv : TEXCOORD0;
	};

	struct v2f
	{
		float2 uv : TEXCOORD0;
		float2 uv2 : TEXCOORD1;
		float4 vertex : SV_POSITION;
	};

	sampler2D _MainTex;
	float4 _MainTex_ST;
	sampler2D _DataTex;
	float4 _DataTex_ST;

	v2f vert(appdata v)
	{
		v2f o;
		o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
		o.uv = TRANSFORM_TEX(v.uv, _MainTex);
		o.uv2 = TRANSFORM_TEX(v.uv, _DataTex);
		return o;
	}

	fixed4 frag(v2f i) : SV_Target
	{
		// sample the texture
		fixed4 col = tex2D(_MainTex, i.uv);

	fixed4 data = tex2D(_DataTex, i.uv2);
	if (data.r > 0.9f)
	{
		return col * (1.0f + data.g * 2.0f);
	}
	else
	{
		clip(-1.0);
		return fixed4(0, 0, 0, 0);
	}
	}
		ENDCG
	}
	}
}
