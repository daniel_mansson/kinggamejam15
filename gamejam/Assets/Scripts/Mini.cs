﻿using UnityEngine;
using System.Collections;

public class Mini : MonoBehaviour
{
    float m_timer;

    MeshRenderer m_renderer;

	// Use this for initialization
	void Start () {
        m_renderer = GetComponent<MeshRenderer>();
        m_timer = UnityEngine.Random.Range(1.5f, 2.0f);
	}

    public void SetColor(Color c)
    {
        m_renderer.material.color = c;
    }
	
	// Update is called once per frame
	void Update ()
    {
        m_timer -= Time.deltaTime;

        if (m_timer < 0.0f)
        {
            Destroy(gameObject);
        }

        float a = Mathf.Clamp01(m_timer * 2.0f);

        Color c = m_renderer.material.color;
        c.a = a;
        m_renderer.material.color = c;
	}
}
