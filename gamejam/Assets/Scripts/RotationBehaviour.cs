﻿using UnityEngine;
using System.Collections;

public class RotationBehaviour : MonoBehaviour
{
    public float rotationSpeed = 1f;

    public float ellipseMajorAxis = 10;
    public float ellipseMinorAxis = 5;

    private float pathIncrementation = 0;
    private readonly int magicalConstant = 625;

    private Vector2 sunPosition;
    // Use this for initialization
    Rigidbody2D m_body;
    void Start()
    {
        m_body = GetComponent<Rigidbody2D>();
        sunPosition = GameObject.Find("Sun").transform.position;
        pathIncrementation = transform.position.x < 0 ? magicalConstant : 0;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        pathIncrementation += rotationSpeed;
        var newX = sunPosition.x + (ellipseMajorAxis * Mathf.Cos(pathIncrementation * 0.005f));
        var newY = sunPosition.y + (ellipseMinorAxis * Mathf.Sin(pathIncrementation * 0.005f));
        //transform.position = new Vector2(newX, newY);
        var target = new Vector2(newX, newY);
        m_body.velocity = (target - m_body.position) / Time.fixedDeltaTime;
    }
}
