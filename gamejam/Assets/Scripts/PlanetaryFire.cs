﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlanetaryFire : MonoBehaviour
{
    public GameObject debris;
    public float debrisFireMagnitude = 11f;
    public readonly int cooldown = 60;
    private int charge = 0;
    private Transform arrow;

    private int cooldownCounter;

    // Use this for initialization
    void Start()
    {
        arrow = transform.GetChild(2);
        cooldownCounter = 0;
    }

    // Update is called once per frame
    void Update()
    {
        var spRend = arrow.GetComponent<SpriteRenderer>();

        Color col = spRend.color;
        //  change col's alpha value (0 = invisible, 1 = fully opaque)
        col.a = 0.5f; // 0.5f = half transparent
                      // change the SpriteRenderer's color property to match the copy with the altered alpha value

        if (cooldownCounter > 0)
            col.a = 0.4f; // 0.5f = half transparent
        else
            col.a = 1f;

        spRend.color = col;

        // arrow.GetComponent<SpriteRenderer>().enabled =
        //  !(cooldownCounter > 0);

        if (cooldownCounter > 0)
        {
            cooldownCounter--;
            GetComponentInChildren<Text>().text = "";
            for (int i = 0; i < cooldownCounter / 6; i++)
            {
                GetComponentInChildren<Text>().text += "-";
            }
        }

    }

    public void FireDebris(Vector2 direction)
    {
        if (cooldownCounter == 0)
        {
            var debris = Instantiate(this.debris, transform.position, Quaternion.identity);
            ((GameObject)debris).GetComponent<Rigidbody2D>().AddForce(direction * debrisFireMagnitude, ForceMode2D.Impulse);
            cooldownCounter = cooldown;
            charge = 0;
            debrisFireMagnitude = 11f;
            arrow.GetComponent<SpriteRenderer>().color = Color.white;
            arrow.localScale = new Vector3(1, 1, 1);
        }

    }

    public void AimArrow(Vector3 lookAt)
    {

        Vector3 diff = lookAt; // - arrow.position;
        //diff.Normalize();

        var angle = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        arrow.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    public void Charge()
    {
        var spRend = arrow.GetComponent<SpriteRenderer>();

        if (cooldownCounter == 0)
        {
            charge++;
            Debug.Log(charge);
            var col = spRend.color;
            col.r += 0.1f;
            spRend.color = col;

            if (charge < 800)
                arrow.localScale += new Vector3(0.005F, 0.001F, 0);

            if (charge > 40 && charge < 140)
            {
                // arrow.GetComponent<SpriteRenderer>().color = Color.darkgre;
                spRend.color = new Color(0, 255 / 255f, 0);
                //debrisFireMagnitude = 12f;

            }

            if (charge > 140 && charge <= 300)
            {
                //  arrow.GetComponent<SpriteRenderer>().color = Color.red;
                spRend.color = new Color(255 / 255f, 255 / 255f, 0 / 255f);
                debrisFireMagnitude = 13f;
                //arrow.localScale += new Vector3(0.2F, 0, 0);
            }

            if (charge > 300 && charge <= 460)
            {
                //  arrow.GetComponent<SpriteRenderer>().color = Color.red;
                spRend.color = new Color(255 / 255f, 140 / 255f, 0 / 255f);
                debrisFireMagnitude = 14;
                // arrow.localScale += new Vector3(0.2F, 0, 0);
            }

            if (charge > 460 && charge <= 660)
            {
                spRend.color = new Color(255 / 255f, 80 / 255f, 60 / 255f);
                //  arrow.GetComponent<SpriteRenderer>().color = Color.red;
                debrisFireMagnitude = 15f;
                //   arrow.localScale += new Vector3(0.2F, 0, 0);
            }

            if (charge > 800)
            {
                spRend.color = new Color(255 / 255f, 0, 0);
                //  arrow.GetComponent<SpriteRenderer>().color = Color.red;
                debrisFireMagnitude = 16f;
                //   arrow.localScale += new Vector3(0.2F, 0, 0);
            }
        }
    }

}
