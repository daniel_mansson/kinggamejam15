﻿using UnityEngine;
using System.Collections;

public class Controls : MonoBehaviour
{
    private Controller controllerA;
    private Controller controllerB;
    [SerializeField]
    private PlanetaryFire planetA;
    [SerializeField]
    private PlanetaryFire planetB;



    // Use this for initialization
    void Start()
    {

        controllerA = Game.Instance.Input.GetController(0);
        controllerB = Game.Instance.Input.GetController(1);
    }

    // Update is called once per frame
    void Update()
    {
        if (controllerA == null || controllerB == null)
            return;

        //------------------- PlayerA -----------------------------
        Vector3 joystickDirectionA = controllerA.GetJoystick(Xbox360ControllerJoystickId.Left).normalized;
        //    Debug.Log(joystickDirection);

        planetA.AimArrow(joystickDirectionA);

        if (controllerA.GetButton(Xbox360ControllerButtonId.X))
        {
            planetA.Charge();
        }

        if (controllerA.GetButtonUp(Xbox360ControllerButtonId.X) && joystickDirectionA.magnitude > 0.2f)
        {
            planetA.FireDebris(joystickDirectionA);
        }

        //------------------- PlayerB -----------------------------
        Vector3 joystickDirectionB = controllerB.GetJoystick(Xbox360ControllerJoystickId.Left).normalized;
        planetB.AimArrow(joystickDirectionB);

        if (controllerB.GetButton(Xbox360ControllerButtonId.X))
        {
            planetB.Charge();
        }

        if (controllerB.GetButtonUp(Xbox360ControllerButtonId.X) && joystickDirectionB.magnitude > 0.2f)
        {
            planetB.FireDebris(joystickDirectionB);
        }

    }

    private bool VerifyIsSignificantMovement(Vector3 joystickDirection)
    {
        if (Mathf.Abs(joystickDirection.x) > 0.5f || Mathf.Abs(joystickDirection.y) > 0.5f || Mathf.Abs(joystickDirection.z) > 0.5f)
            return true;

        return false;
    }

}
