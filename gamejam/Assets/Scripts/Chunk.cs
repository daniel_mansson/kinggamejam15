﻿using UnityEngine;
using System.Collections;

public class Chunk : MonoBehaviour
{
    [SerializeField]
    public ChunkData m_data;

    [SerializeField]
    public MeshRenderer m_visual;

    [SerializeField]
    public int m_id = 0;

    [SerializeField]
    GameObject m_explosionPrefab;

    public float gravityScale = 1.0f;
    
	void Start ()
    {
    }


    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mpos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            var expl = (GameObject)Instantiate(m_explosionPrefab, mpos, Quaternion.identity);
            m_data.BlowShitUp(mpos, 10.0f);
        }
    }

    void OnCollisionEnter2D(Collision2D _collision)
    {
        var proj = _collision.collider.GetComponent<Projectile>();

        if (proj == null || !proj.IsArmed)
            return;

        m_data.BlowShitUp(_collision.contacts[0].point, 10.0f);
        var dir = (Vector2)(_collision.contacts[0].point - (Vector2)transform.position);
        var expl = (GameObject)Instantiate(m_explosionPrefab, _collision.contacts[0].point, Quaternion.LookRotation(dir, Vector3.forward));
        expl.transform.parent = this.transform;
    }
}
