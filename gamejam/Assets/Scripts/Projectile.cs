﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    public int m_id = 0;
    public float m_timer = 0.1f;
    [SerializeField]
    Collider2D m_collider;

    public bool IsArmed
    {
        get { return m_timer < 0.0f; }
    }

    void Awake()
    {
        m_collider.enabled = false;
    }

    void Update()
    {
        m_timer -= Time.deltaTime;

        m_collider.enabled = IsArmed;
    }

    void OnCollisionEnter2D(Collision2D _collision)
    {
        if (!IsArmed)
            return;

        Destroy(this.gameObject);
    }
}
