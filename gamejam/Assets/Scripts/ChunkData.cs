﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ChunkData : MonoBehaviour
{
    [SerializeField]
    MeshRenderer m_visualRenderer;

    [SerializeField]
    MeshRenderer m_dataRenderer;

    [SerializeField]
    public int m_dataResolutionX = 256;
    [SerializeField]
    public int m_dataResolutionY = 256;

    [SerializeField]
    PolygonCollider2D m_poly;

    [SerializeField]
    Chunk m_chunkPrefab;

    [SerializeField]
    Mini m_miniPrefab;

    [SerializeField]
    Texture2D m_planetTexture;

    [SerializeField]
    public bool m_fillWithCircle = true;

    public Texture2D Texture { get { return m_dataTex; } }

    Texture2D m_dataTex;

    int m_lastFloodId = 0;
    int m_lastVisitedId = 0;
    struct Cell
    {
        public int visitedId;
        public int floodId;
    };

    Cell[] m_cells;
    Color32[] m_data;

    int[] m_move;

    int CellIndex(int _x, int _y)
    {
        return _y * m_dataResolutionX + _x;
    }

    bool init = false;
    public void Init()
    {
        if (init)
            return;
        init = true;

        m_dataTex = new Texture2D(m_dataResolutionX, m_dataResolutionY, TextureFormat.RGBA32, false);
        m_dataTex.filterMode = FilterMode.Point;

        m_data = m_dataTex.GetPixels32();
        m_cells = new Cell[m_dataResolutionX * m_dataResolutionY];

        m_move = new int[] { 1, -m_dataResolutionX, -1, m_dataResolutionX };

        int firstSolid = -1;

        if (m_fillWithCircle)
        {
            Vector2 center = new Vector2(m_dataResolutionX / 2, m_dataResolutionY / 2);
            for (int i = 0; i < m_dataResolutionY; i++)
            {
                for (int j = 0; j < m_dataResolutionX; j++)
                {
                    Vector2 pos = new Vector2(j + 0.5f, i + 0.5f);
                    float d = Vector2.Distance(pos, center);
                    float border = 3.0f;
                    int idx = CellIndex(j, i);

                    if (d < m_dataResolutionX / 2.78f - 1 - border)
                    {
                        m_data[idx] = new Color32(255, 0, 0, 255);

                        if (firstSolid == -1)
                        {
                            firstSolid = idx;
                        }
                    }
                    else
                        m_data[idx] = new Color32(0, 0, 0, 0);
                }
            }

            int dir = Up;

            FindEdge(firstSolid, dir);
            UpdateCollider();

            foreach (var edgeIdx in m_edge)
            {
                m_data[edgeIdx].g = (byte)255;
            }
        }

        m_dataTex.SetPixels32(m_data);
        m_dataTex.Apply();

        m_dataRenderer.material.mainTexture = m_dataTex;
        m_visualRenderer.material.SetTexture("_DataTex", Texture);
    }

    void Start ()
    {
        Init();
    }

    void UpdateCollider()
    {
        int scale = 8;
        Vector2[] points = new Vector2[m_edge.Count/ scale];

        for (int i = 0; i < points.Length; i++)
        {
            int p = m_edge[i * scale];
            points[i] = new Vector2(p % m_dataResolutionX, p / m_dataResolutionX) - Vector2.one * 0.5f;
            points[i].x /= m_dataResolutionX;
            points[i].y /= m_dataResolutionY;
            points[i].x -= 0.5f;
            points[i].y -= 0.5f;
        }

        m_poly.points = points;
    }

    List<int> m_edge = new List<int>();

    const int Right = 0;
    const int Down = 1;
    const int Left = 2;
    const int Up = 3;

    int Ccw(int _dir)
    {
        return (_dir + 3) % 4;
    }

    int Cw(int _dir)
    {
        return (_dir + 1) % 4;
    }

    int Move(int _idx, int _dir)
    {
        return _idx + m_move[_dir];
    }

    bool IsOutside(int _idx)
    {
        return _idx < 0 || _idx >= m_dataResolutionX * m_dataResolutionY;
    }

    bool IsCellToTheLeftSolid(int _idx, int _dir)
    {
        int pos = Move(_idx, Ccw(_dir));

        if (IsOutside(pos))
            return false;

        return m_data[pos].r != 0;
    }

    bool IsNextCellSolid(int _idx, int _dir)
    {
        int pos = Move(_idx, _dir);

        if (IsOutside(pos))
            return false;

        return m_data[pos].r != 0;
    }

    List<int> FindEdge(int startIdx, int startDir)
    {
        m_edge.Clear();
        ++m_lastVisitedId;

        int pos = startIdx;
        int dir = startDir;

        bool done = false;

        while (!done)
        {
            if (IsOutside(pos))
                break;

            m_cells[pos].visitedId = m_lastVisitedId;
            m_edge.Add(pos);

            int newDir = Ccw(dir);
            for (int i = 0; i < 4; i++)
            {
                if (IsNextCellSolid(pos, newDir))
                {
                    dir = newDir;
                    break; 
                }

                newDir = Cw(newDir);
            }

            pos = Move(pos, dir);

            if (pos == startIdx)
            {
                //FULL CIRCLE Whoaoao
                break;
            }
        }

        return m_edge;
    }


    public void BlowShitUp(Vector3 _worldPos, float _size)
    {
        Vector2 lpos = transform.InverseTransformPoint(_worldPos);

        float rad = m_dataResolutionX / 10.0f;
        lpos += Vector2.one * 0.5f;
        Vector2 center = new Vector2(lpos.x * m_dataResolutionX, lpos.y * m_dataResolutionY);
        for (int i = 0; i < m_dataResolutionY; i++)
        {
            for (int j = 0; j < m_dataResolutionX; j++)
            {
                Vector2 pos = new Vector2(j + 0.5f, i + 0.5f);

                int idx = CellIndex(j, i);

                float d = Vector2.Distance(pos, center);
                float border = 3.0f;

                if (d < rad - border && m_data[idx].r != 0)
                {
                    pos -= new Vector2(m_dataResolutionX * 0.5f, m_dataResolutionY * 0.5f);

                    Vector2 np = transform.position + transform.rotation * (pos / 128.0f);
                    var mini = (Mini)Instantiate(m_miniPrefab, np, transform.rotation);
                    mini.transform.localScale = Vector2.one * (2.0f / 256.0f);

                    Vector2 dir = _worldPos - transform.position;

                    float t = Mathf.Clamp01(d / rad);
                    float pow = Mathf.Lerp(0.8f, 0.0f, t) * UnityEngine.Random.Range(0.7f, 1.3f);

                    if (UnityEngine.Random.Range(0.0f, 1.0f) < 0.05f)
                        pow *= 1.7f;

                    dir.Normalize();
                    var mbody = mini.GetComponent<Rigidbody2D>();

                    dir += UnityEngine.Random.insideUnitCircle * 0.2f;

                    var b = transform.parent.GetComponent<Rigidbody2D>();
                    var vel = b.GetRelativePointVelocity(b.transform.InverseTransformPoint(mbody.position));

                    mbody.velocity = pow * dir + vel;

                    mini.GetComponent<MeshRenderer>().material.color = m_planetTexture.GetPixelBilinear((float)j / 256.0f, (float)i / 256.0f);

                    m_data[idx] = new Color32(0, 0, 0, 0);
                }
            }
        }

        FindParts();

        int largesCount = -1;
        Part largest = null;
        foreach (var p in m_parts)
        {
            if (p.count > largesCount)
            {
                largesCount = p.count;
                largest = p;
            }

        }

        //Just remove shit
        foreach (var p in m_parts)
        {
            if (p != largest)
            {
                int w = 1 + p.maxX - p.minX;
                int h = 1 + p.maxY - p.minY;
                int s = Mathf.Max(w, h);

                Vector2 cen = new Vector2(
                    (1 + p.minX + p.maxX) * 0.5f * (2.0f / m_dataResolutionX), 
                    (1 + p.minY + p.maxY) * 0.5f * (2.0f / m_dataResolutionY)) - Vector2.one;

                var newChunk = (Chunk)Instantiate(m_chunkPrefab, transform.position + transform.rotation * cen, transform.rotation);

                newChunk.transform.localScale = new Vector3(
                    transform.lossyScale.x * (float)w / (float)m_dataResolutionX, 
                    transform.lossyScale.y * (float)h / (float)m_dataResolutionY, 
                    0);
                newChunk.m_data.m_fillWithCircle = false;
                newChunk.m_data.m_dataResolutionX = w;
                newChunk.m_data.m_dataResolutionY = h;
                newChunk.m_data.Init();

                newChunk.m_visual.material.mainTexture = m_visualRenderer.material.mainTexture;
                Vector2 uvs = new Vector2((float)w / (float)m_dataResolutionX, (float)h / (float)m_dataResolutionY); 
                newChunk.m_visual.material.mainTextureScale = uvs;
                Vector2 uvc = Vector2.zero;// new Vector2((1.0f - uvs.x) * 0.5f, (1.0f - uvs.y) * 0.5f);
                float o = (float)p.minX / (float)m_dataResolutionX;
                uvc.x += o ;// * (float)p.minX / (float)m_dataResolutionX + 0.5f;

                o = (float)p.minY / (float)m_dataResolutionY;
                uvc.y += o ;// * (float)p.minY / (float)m_dataResolutionY + 0.5f;
                newChunk.m_visual.material.mainTextureOffset = uvc;

                for (int i = 0; i < newChunk.m_data.m_dataResolutionY; i++)
                {
                    for (int j = 0; j < newChunk.m_data.m_dataResolutionX; j++)
                    {
                        int idx = newChunk.m_data.CellIndex(j, i);
                        newChunk.m_data.m_data[idx] = new Color32(0,0,0,0);
                    }
                }

                        int c = 0;
                FloodFillHeap(p.firstEdge, ++m_lastFloodId, (idx) =>
                {
                    int x = idx % m_dataResolutionX;
                    int y = idx / m_dataResolutionX;
                    int nx = x - p.minX;
                    int ny = y - p.minY;
                    int nidx = newChunk.m_data.CellIndex(nx, ny);
                    if (!newChunk.m_data.IsOutside(nidx))
                    {
                        newChunk.m_data.m_data[nidx].r = 255;
                        newChunk.m_data.m_data[nidx].a = 255;
                    }

                    m_data[idx].r = 0;
                    m_data[idx].g = 0;
                    m_data[idx].b = 0;
                    m_data[idx].a = 0;
                    ++c;
                });
                Debug.Log("Kill: " + c);

                newChunk.m_data.FindEdgeAndUpdate();

                newChunk.m_data.m_dataTex.SetPixels32(newChunk.m_data.m_data);
                newChunk.m_data.m_dataTex.Apply();

                var body = transform.parent.GetComponent<Rigidbody2D>();
                var newbody = newChunk.GetComponent<Rigidbody2D>();
                var vel = body.GetRelativePointVelocity(body.transform.InverseTransformPoint(newbody.position));
                vel += (newbody.position - body.position).normalized + UnityEngine.Random.insideUnitCircle.normalized * 0.5f * UnityEngine.Random.Range(0.5f, 2.0f);
                newbody.velocity = vel;
                newbody.angularVelocity = UnityEngine.Random.Range(-40.0f, 40.0f);
            }
        }

        bool amIAlive = FindEdgeAndUpdate();

        if (!amIAlive)
        {
            Destroy(transform.parent.gameObject);
        }

        m_dataTex.SetPixels32(m_data);
        m_dataTex.Apply();
    }

    bool FindEdgeAndUpdate()
    {
        int first = -1;
        for (int i = 0; i < m_dataResolutionY; i++)
        {
            for (int j = 0; j < m_dataResolutionX; j++)
            {
                int idx = CellIndex(j, i);
                if (m_data[idx].r > 0)
                {
                    first = idx;
                    break;
                }
            }
        }

        if (first == -1)
            return false;

        int dir = 0;
        if (!IsNextCellSolid(first, Up) && IsNextCellSolid(first, Right))
            dir = Right;
        if (!IsNextCellSolid(first, Right) && IsNextCellSolid(first, Down))
            dir = Down;
        if (!IsNextCellSolid(first, Down) && IsNextCellSolid(first, Left))
            dir = Left;
        if (!IsNextCellSolid(first, Left) && IsNextCellSolid(first, Up))
            dir = Up;

        foreach (var edgeIdx in m_edge)
        {
            m_data[edgeIdx].g = (byte)0;
        }

        FindEdge(first, dir);
        UpdateCollider();


        foreach (var edgeIdx in m_edge)
        {
            m_data[edgeIdx].g = (byte)255;
        }


        return true;
    }

    List<Part> m_parts = new List<Part>();
    class Part
    {
        public int id;
        public int firstEdge;
        public int count;
        public int minX = 10000000;
        public int maxX = -1;
        public int minY = 10000000;
        public int maxY = -1;
    }

    void FindParts()
    {
        m_lastFloodId++;
        int startFlood = m_lastFloodId;

        m_parts.Clear();

        for (int i = 0; i < m_dataResolutionY; i++)
        {
            for (int j = 0; j < m_dataResolutionX; j++)
            {
                int idx = CellIndex(j, i);

                if (m_data[idx].r != 0 && m_cells[idx].floodId < startFlood)
                {
                    Part part = new Part();
                    part.firstEdge = -1;
                    part.id = m_lastFloodId;

                    FloodFillHeap(idx, m_lastFloodId, (onIdx) => 
                    {
                        part.count++;

                        if (part.firstEdge == -1 && !IsNextCellSolid(onIdx, Left))
                            part.firstEdge = onIdx;

                        int x = onIdx % m_dataResolutionX;
                        int y = onIdx / m_dataResolutionX;

                        if (x < part.minX)
                            part.minX = x;
                        if (x > part.maxX)
                            part.maxX = x;
                        if (y < part.minY)
                            part.minY = y;
                        if (y > part.maxY)
                            part.maxY = y;
                    });

                    m_parts.Add(part);
                    m_lastFloodId++;
                }
            }
        }
    }

    List<int> m_floodQueue = new List<int>();
    void FloodFillHeap(int _idx, int _id, Action<int> _onCell)
    {
        m_floodQueue.Clear();

        m_cells[_idx].floodId = _id;
        m_floodQueue.Add(_idx);

        while (m_floodQueue.Count != 0)
        { 
            int last = m_floodQueue[m_floodQueue.Count - 1];
            m_floodQueue.RemoveAt(m_floodQueue.Count - 1);

            _onCell(last);

            for (int i = 0; i < 4; i++)
            {
                int pos = last + m_move[i];

                if (IsOutside(pos))
                    continue;

                if (m_data[pos].r == 0)
                    continue;

                if (m_cells[pos].floodId == _id)
                    continue;

                m_cells[pos].floodId = _id;
                m_floodQueue.Add(pos);
            }
        }
    }

}
