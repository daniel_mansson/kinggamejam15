﻿using UnityEngine;
using System.Collections;

public class TestGunBehaviour : MonoBehaviour
{

    public int fireStrength = 500;
    private Controller controller;

    // Use this for initialization
    void Start()
    {

        controller = Game.Instance.Input.GetController(0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("Are we here");
            Fire();
        }

    }

    void Fire()
    {
        var target = Camera.main.ScreenToWorldPoint(Input.mousePosition);



        GetComponent<Rigidbody2D>().AddForce((target - transform.position).normalized * fireStrength, ForceMode2D.Impulse);
    }

}
