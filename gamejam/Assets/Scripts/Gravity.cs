﻿using UnityEngine;
using System.Collections;

public class Gravity : MonoBehaviour
{

    public float maxGravityDistance = 4.0f;
    public float gravityMagnitude = 35.0f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }

    void FixedUpdate()
    {
        var gravitons = GameObject.FindGameObjectsWithTag("Graviton");

        foreach (GameObject graviton in gravitons)
        {
            float dist = Vector3.Distance(transform.position, graviton.transform.position);
            if (dist <= maxGravityDistance)
            {
                Vector3 gravityPullDirection = transform.position - graviton.transform.position;

                var gravitonRigidbody = graviton.GetComponent<Rigidbody2D>();
                if (gravitonRigidbody == null)
                    Debug.Log("Object " + graviton.name + " is graviton but has no rigidbody.");

                gravitonRigidbody.AddForce(gravityPullDirection.normalized * (1.0f - dist / maxGravityDistance) * gravityMagnitude);
            }
        }
    }
}
